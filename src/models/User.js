import axios from "axios";
export default class User {
  isLoading = true;
  constructor() {}

  load() {
    this.isLoading = true;
    var id = localStorage.getItem("user");
    if (id) {
      return axios
        .post("http://127.0.0.1:8000/api/v1/user/detail", { id: id })
        .then(response => {
          this.setAttribute(response.data);
        });
    }
  }

  setToken(token) {
    localStorage.setItem("user", token);
  }

  getToken() {
    return localStorage.getItem("user");
  }

  removeToken() {
    localStorage.removeItem("user");
  }

  setAttribute(data) {
    for (let k in data) {
      this[k] = data[k];
    }
  }

  reset() {
    this.isLoading = true;
    for (let k in this) {
      this[k] = null;
    }
    this.isLoading = false;
  }
}
