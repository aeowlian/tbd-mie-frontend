export default class Flash {
  show = false;
  message = null;
  linkStyle = "color:inherit;font-size:inherit;";
  actionStyle = "color:inherit;font-size:inherit;";
  url = null;
  link = null;
  button = null;
  type = null;

  constructor() {}

  messages(message, type = "info") {
    this.show = true;
    this.message = " " + message;
    this.type = type;
  }

  resetAction() {
    this.link = null;
    this.url = null;
    this.button = null;
  }

  addLink(link, url) {
    this.link = link;
    this.url = url;
  }

  addButton(button, url) {
    this.url = url;
    this.button = button;
  }

  danger(message) {
    this.messages(message, "danger");
  }

  success(message) {
    this.messages(message, "success");
  }

  info(message) {
    this.messages(message, "info");
  }
}
