import axios from "axios";
import TokenService from "@/common/token.service";
import { API_URL } from "@/common/config";

const ApiService = {
  init() {
    axios.defaults.baseURL = API_URL;
  },

  query(resource, params) {
    return axios.get(resource, params).catch(error => {
      throw new Error(`ApiService ${error}`);
    });
  },

  get(resource, slug = "") {
    return axios.get(`${resource}${slug}`).catch(error => {
      throw new Error(`ApiService ${error}`);
    });
  },

  post(resource, params) {
    return axios.post(`${resource}`, params);
  },

  update(resource, slug, params) {
    return axios.put(`${resource}/${slug}`, params);
  },

  put(resource, params) {
    return axios.put(`${resource}`, params);
  },

  delete(resource) {
    return axios.delete(resource).catch(error => {
      throw new Error(`ApiService ${error}`);
    });
  }
};

export default ApiService;

// export const ProblemsetsService = {
//   get (slug) {
//     return ApiService.get("problemsets", slug)
//   },
//   create (params) {
//     return ApiService.post('articles', {article: params})
//   },
//   update (slug, params) {
//     return ApiService.update('articles', slug, {article: params})
//   },
//   destroy (slug) {
//     return ApiService.delete(`articles/${slug}`)
//   }
// }
