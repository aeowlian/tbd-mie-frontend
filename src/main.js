import Vue from "vue";
import "./plugins/vuetify";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import ApiService from "./common/api.service";

Vue.config.productionTip = false;

Vue.use(Vuetify);

ApiService.init();

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
