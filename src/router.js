import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/About.vue")
    },
    {
      path: "/login",
      name: "login",
      // route level code-splitting
      // this generates a separate chunk (login.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "login" */ "./views/Login.vue")
    },
    {
      path: "/order",
      name: "order",
      // route level code-splitting
      // this generates a separate chunk (order.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "order" */ "./views/Order.vue")
    },
    {
      path: "/forget-password",
      name: "forget-password",
      // route level code-splitting
      // this generates a separate chunk (order.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "order" */ "./views/SendToken.vue")
    },
    {
      path: "/reset-password",
      name: "reset-password",
      // route level code-splitting
      // this generates a separate chunk (order.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "order" */ "./views/Verify.vue")
    },
    {
      path: "/register",
      name: "register",
      // route level code-splitting
      // this generates a separate chunk (order.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "order" */ "./views/Register.vue")
    },
    {
      path: "/report",
      name: "report",
      // route level code-splitting
      // this generates a separate chunk (order.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "order" */ "./views/Report.vue")
    },
    {
      path: "/management",
      name: "management",
      // route level code-splitting
      // this generates a separate chunk (order.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
      import(/* webpackChunkName: "order" */ "./views/Management.vue")
    }
  ]
});
